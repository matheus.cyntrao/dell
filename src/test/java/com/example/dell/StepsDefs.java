package com.example.dell;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.web.client.RestTemplate;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepsDefs extends SpringIntegrationTest {

    // marca: GM - chevrolet (23), modelo: CRUZE LTZ 1.8 16V FlexPower 4p Aut. (5637), ano 2016.

    private static String marcas = "https://parallelum.com.br/fipe/api/v1/carros/marcas";
    private static String modelos = "https://parallelum.com.br/fipe/api/v1/carros/marcas/59/modelos";
    private static String anos = "https://parallelum.com.br/fipe/api/v1/carros/marcas/59/modelos/5940/anos";
    private static String valores = "https://parallelum.com.br/fipe/api/v1/carros/marcas/59/modelos/5940/anos/2014-3";
    private String valorComparacao, valor = "R$ 0000.00";
    ObjectMapper mapper = new ObjectMapper();

    int codigoMarca, codigoModelo;
    String codigoAno;

    /*
        Dado que eu sei marca do carro
        E o modelo
        E o ano do veículo
        Quando eu realizar uma consulta
        Então é possível obter o seu valor atual de mercado.
    */

    @Given("^que eu sei marca do carro$")
    public void dado_que_eu_sei_marca_do_carro() throws Throwable {
        RestTemplate restTemplate = new RestTemplate();
        String uri = "https://parallelum.com.br/fipe/api/v1/carros/marcas";
        String result = restTemplate.getForObject(uri, String.class);
        Marca[] marcas_ = mapper.readValue(result, Marca[].class);
        List<Marca> list = Arrays.asList(marcas_);
        this.codigoMarca = Utils.getCodigoMarca(list, "GM - Chevrolet");
    }

    @Given("^o modelo$")
    public void o_modelo() throws Throwable {
        RestTemplate restTemplate = new RestTemplate();
        String uri = "https://parallelum.com.br/fipe/api/v1/carros/marcas/"+this.codigoMarca+"/modelos";
        String result = restTemplate.getForObject(uri, String.class).replace("{\"modelos\":","").replace("]}", "]");
        Modelo[] modelos_ = mapper.readValue(result, Modelo[].class);
        List<Modelo> list = Arrays.asList(modelos_);
        this.codigoModelo = Utils.getCodigoModelo(list, "CRUZE LTZ 1.8 16V FlexPower 4p Aut.");
    }

    @Given("^o ano do veículo$")
    public void o_ano() throws Throwable {
        RestTemplate restTemplate = new RestTemplate();
        String uri = "https://parallelum.com.br/fipe/api/v1/carros/marcas/"+this.codigoMarca+"/modelos/"+this.codigoModelo+"/anos";
        String result = restTemplate.getForObject(uri, String.class);
        Ano[] anos_ = mapper.readValue(result, Ano[].class);
        List<Ano> list = Arrays.asList(anos_);
        this.codigoAno = Utils.getCodigoAno(list, "2016");
    }

    @When("^eu realizar uma consulta$")
    public void eu_realizar_uma_consulta() throws Throwable {
        RestTemplate restTemplate = new RestTemplate();
        String uri = "https://parallelum.com.br/fipe/api/v1/carros/marcas/"+this.codigoMarca+"/modelos/"+this.codigoModelo+"/anos/"+this.codigoAno+"";
        String result = restTemplate.getForObject(uri, String.class);
        System.out.println(result);
    }

    @Then("^é possível obter o seu valor atual de mercado$")
    public void o_saldo_devera_ser_atualizado() throws Throwable {
        RestTemplate restTemplate = new RestTemplate();
        String uri = "https://parallelum.com.br/fipe/api/v1/carros/marcas/"+this.codigoMarca+"/modelos/"+this.codigoModelo+"/anos/"+this.codigoAno+"";
        String result = restTemplate.getForObject(uri, String.class);
        Carro carro = mapper.readValue(result, Carro.class);
        assertTrue(carro.Valor.equalsIgnoreCase("R$ 59.766,00"));
    }

}