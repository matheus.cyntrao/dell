package com.example.dell;

import java.util.List;

public class Utils {

    public static int getCodigoMarca(List<Marca> list, String name) {
        for(Marca marca : list) {
            if(name.equalsIgnoreCase(marca.nome))
                return marca.codigo;
        }
        return -1;
    }

    public static int getCodigoModelo(List<Modelo> list, String name) {
        for(Modelo modelo : list) {
            if(name.equalsIgnoreCase(modelo.nome))
                return modelo.codigo;
        }
        return -1;
    }

    public static String getCodigoAno(List<Ano> list, String anos) {
        for(Ano ano : list) {
            if(ano.nome.contains(anos))
                return ano.codigo;
        }
        return "-1";
    }

}