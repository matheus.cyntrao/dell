package com.example.dell;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"src/test/resources"}, glue = {"com.example.dell"})
public class CucumberTest extends SpringIntegrationTest {
}
